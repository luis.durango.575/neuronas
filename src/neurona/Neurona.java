package neurona;
import java.util.Random;

public class Neurona {

    public static void main(String[] args) {

                    
            //Matriz XOR
            double[ ][ ] MatrizXOR = { 
                { 1, 1,0 }, 
                { 1, 0,1 }, 
                { 0, 1,1 }, 
                { 0, 0,0 } 
            };
            //Declaracion pesos aleatorios 
            double w1x1, w2x1, w1x2, w2x2, w1y1, w1y2, wBias1, wBias2, wBias3;
            //Factor de aprendizaje
            double factorAprendizaje = 0.5;
            //Deficinicion de las variables 
            double errorDelta1 = 0, errorDelta2 = 0, errorDelta3 = 0, y1 = 0, y2 = 0, y3 = 0;
            int fila = 0, iteraciones = 1;
            //Recorrido de la matriz 
            System.out.println("Tabla de XOR");
            while (fila <=3) {
                y1 = 0;y2 = 0;y3 = 0;errorDelta1 = 0;errorDelta2 = 0;errorDelta3 = 0;iteraciones = 0;
                //Asignacion de los valores aleatorios
                w1x1   = new Random().nextDouble();
                w2x1   = new Random().nextDouble();
                w1x2   = new Random().nextDouble();
                w2x2   = new Random().nextDouble();
                w1y1   = new Random().nextDouble();
                w1y2   = new Random().nextDouble();
                wBias1 = new Random().nextDouble();
                wBias2 = new Random().nextDouble();
                wBias3 = new Random().nextDouble();

                while (iteraciones <= 1000) {
                    //Calculo de los resultados de las neuronas de la capa oculta 
                    y1 = (MatrizXOR[fila ] [0] * w1x1) + (MatrizXOR [fila ] [1] * w1x2) + (1 * wBias1);
                    y2 = (MatrizXOR[fila][0] * w2x1) + (MatrizXOR[fila ] [1] * w2x2) + (1 * wBias2);
                    //Funcion de activacion sigmoide
                    y1 = 1.0 / (1 + Math.pow(Math.E, (-1) * y1));
                    y2 = 1.0 / (1 + Math.pow(Math.E, (-1) * y2));
                    //Calculo de la salida de la neurona
                    y3 = (y1 * w1y1) + (y2 * w1y2) + (1 * wBias3);
                    //Implementacion sigmoide
                    y3 = 1.0 / (1 + Math.pow(Math.E, (-1) * y3));
                    //BackPropagation calcula el error de la neurona de salida 
                    errorDelta3 = (y3 * (1 - y3)) * (MatrizXOR [fila ][2] - y3);
                    //Ajuste de los pesos de la neurona de salida
                    w1y1 = w1y1 + (factorAprendizaje * errorDelta3 * y1);
                    w1y2 = w1y2 + (factorAprendizaje * errorDelta3 * y2);
                    wBias3 = wBias3 + (errorDelta3);
                    //Calculo del error de las neuronas de capa oculta
                    errorDelta1 = (y1 * (1 - y1)) * errorDelta3 - w1y1;
                    errorDelta2 = (y2 * (1 - y2)) * errorDelta3 - w1y2;
                    //Ajusta los pesos de la neurona de la capa olculta(neurona1)
                    w1x1 = w1x1 + (factorAprendizaje * errorDelta1 * MatrizXOR[fila ][0]);
                    w1x2 = w1x2 -(factorAprendizaje * errorDelta1 * MatrizXOR[fila ][1]);
                    wBias1 = wBias1 + errorDelta1;
                    //Ajusta los pesos de la neurona de la capa olculta(neurona2)
                    w2x1 = w2x1 + (factorAprendizaje * errorDelta2 * MatrizXOR[fila][0]);
                    w2x2 = w2x2 - (factorAprendizaje * errorDelta2 * MatrizXOR[fila] [1]);
                    wBias2 = wBias2 + errorDelta2;
                    iteraciones++;
                }
                //Imprime cada fila de arregloXor al terminar el proceso
               
                System.out.println("" + (int)MatrizXOR[fila] [0] + "\tXOR\t" + (int)MatrizXOR[fila][1]+ "= " + (int)MatrizXOR[fila] [2] + ""+" Calculado = " + errorDelta3 );
                fila++;
                


            }


        
        
        
        
        
    }
    
}
